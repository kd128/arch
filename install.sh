#!/bin/sh

disk='vda'
user='user'
host='arch'
zone='UTC'
repo='https://gitlab.com/kd128/arch'

root_conf='/root/.config'
user_home="/home/${user}"
user_docs="${user_home}/documents"
user_conf="${user_home}/.config"
repo_raw="${repo}/-/raw/master"
repo_dir="${user_docs}/arch"

set -e

pacman_install() {
	pacman --sync --needed --noconfirm "${@}"
}

change_ownership() {
	chown --recursive "${user}":"${user}" "${user_home}"
}

configure_pacman() {
	sed --in-place 's/^#Color$/Color/' /etc/pacman.conf
	sed --in-place 's/^#VerbosePkgLists$/VerbosePkgLists/' /etc/pacman.conf
	# shellcheck disable=SC2016
	echo 'Server = https://geo.mirror.pkgbuild.com/$repo/os/$arch' > /etc/pacman.d/mirrorlist
	pacman --sync --refresh
}

configure_time() {
	ln --symbolic --force "/usr/share/zoneinfo/${zone}" /etc/localtime
	hwclock --systohc
	systemctl enable systemd-timesyncd.service
}

configure_locale() {
	sed --in-place 's/^#en_US.UTF-8\ UTF-8.*$/en_US.UTF-8\ UTF-8/' /etc/locale.gen
	locale-gen
	echo 'LANG=en_US.UTF-8' > /etc/locale.conf
	echo 'LC_COLLATE=C' >> /etc/locale.conf
}

configure_network() {
	pacman_install networkmanager
	echo "${host}" > /etc/hostname
	echo '127.0.0.1 localhost' > /etc/hosts
	echo '::1 localhost' >> /etc/hosts
	echo "127.0.1.1 ${host}" >> /etc/hosts
	systemctl enable NetworkManager.service
}

generate_initramfs() {
	mkinitcpio --allpresets
}

configure_users() {
	pacman_install sudo
	echo 'root:root' | chpasswd
	passwd --expire root
	echo '%wheel ALL=(ALL:ALL) ALL' > /etc/sudoers.d/wheel
	useradd "${user}" --create-home --groups='wheel'
	echo "${user}:${user}" | chpasswd
	passwd --expire "${user}"
}

configure_shell() {
	pacman_install bash-completion
	curl "${repo_raw}/shell/root.sh" > /root/.bashrc
	curl "${repo_raw}/shell/user.sh" > "${user_home}/.bashrc"
	curl "${repo_raw}/shell/profile.sh" > "${user_home}/.bash_profile"
	rm --force "${user_home}/.bash_logout"
}

configure_nvim() {
	pacman_install neovim
	mkdir --parents "${root_conf}/nvim"
	curl "${repo_raw}/nvim/init.lua" > "${root_conf}/nvim/init.lua"
	mkdir --parents "${user_conf}/nvim"
	curl "${repo_raw}/nvim/init.lua" > "${user_conf}/nvim/init.lua"
}

configure_grub() {
	pacman_install grub

	if [ -d '/sys/firmware/efi' ]
	then
		pacman_install efibootmgr
		grub-install --efi-directory='/boot'
	else
		grub-install "/dev/${disk}"
	fi

	sed --in-place 's/^GRUB_TIMEOUT=5$/GRUB_TIMEOUT=0/' /etc/default/grub
	grub-mkconfig --output='/boot/grub/grub.cfg'
}

configure_git() {
	pacman_install git less
	mkdir "${user_conf}/git"
	curl "${repo_raw}/git/config" > "${user_conf}/git/config"
}

configure_home_dirs() {
	pacman_install xdg-user-dirs
	user_dirs="
	documents
	documents/desktop
	documents/public
	documents/templates
	downloads
	music
	pictures
	pictures/screenshots
	videos
	"
	# shellcheck disable=SC2086
	cd "${user_home}" && mkdir --parents ${user_dirs}
	git clone "${repo}" "${repo_dir}"
	cp "${repo_dir}/xdg/user-dirs.dirs" "${user_conf}/user-dirs.dirs"
	xdg-user-dirs-update
}

configure_font() {
	pacman_install ttf-ibm-plex
	mkdir "${user_conf}/fontconfig"
	cp "${repo_dir}/fontconfig/fonts.conf" "${user_conf}/fontconfig/fonts.conf"
}

install_xfce() {
	packages="
	xorg-server
	xfwm4
	xfdesktop
	xfce4-appfinder
	xfce4-panel
	xfce4-power-manager
	xfce4-session
	xfce4-settings
	xfce4-terminal
	xfce4-screensaver
	network-manager-applet
	pulseaudio
	xfce4-pulseaudio-plugin
	pavucontrol
	thunar
	tumbler
	xclip
	firefox
	firefox-ublock-origin
	scrot
	nsxiv
	"
	# shellcheck disable=SC2086
	pacman_install ${packages}
}

configure_qt() {
	pacman_install qt5ct qt6ct papirus-icon-theme
	echo "QT_QPA_PLATFORMTHEME=qt6ct" > /etc/environment
	cd "${user_conf}" && mkdir qt5ct qt6ct
	cp "${repo_dir}/qt/qt.conf" "${user_conf}/qt5ct/qt5ct.conf"
	cp "${repo_dir}/qt/qt.conf" "${user_conf}/qt6ct/qt6ct.conf"
}

configure_mpv() {
	pacman_install mpv yt-dlp
	cd "${user_conf}" && mkdir mpv yt-dlp
	cp "${repo_dir}/mpv/mpv.conf" "${user_conf}/mpv/mpv.conf"
	cp "${repo_dir}/yt-dlp/config" "${user_conf}/yt-dlp/config"
}

configure_coode() {
	packages="
	code
	ccls
	python-pylint
	python-black
	python-isort
	go
	gopls
	staticcheck
	delve
	nodejs-lts-iron
	pnpm
	prettier
	"
	# shellcheck disable=SC2086
	pacman_install ${packages}
	mkdir --parents "${user_conf}/Code - OSS/User" "${user_docs}/go"
	cp "${repo_dir}/code/settings.json" "${user_conf}/Code - OSS/User/settings.json"
}

install_texlive() {
	packages="
	texlive-binextra
	texlive-latexextra
	texlive-fontsrecommended
	texlive-xetex
	perl-yaml-tiny
	perl-file-homedir
	"
	# shellcheck disable=SC2086
	pacman_install ${packages}
}

configure_docker() {
	pacman_install docker docker-compose
	mkdir "${user_docs}/docker"
	usermod "${user}" --append --groups='docker'
	systemctl enable docker.service
}

enable_lightdm() {
	pacman_install lightdm lightdm-gtk-greeter lightdm-gtk-greeter-settings
	systemctl enable lightdm.service
}

install_minimal() {
	configure_pacman
	configure_time
	configure_locale
	configure_network
	generate_initramfs
	configure_users
	configure_shell
	configure_nvim
	configure_grub
	change_ownership
}

install_desktop() {
	configure_git
	configure_home_dirs
	configure_font
	install_xfce
	configure_qt
	configure_mpv
	# configure_coode
	# install_texlive
	# configure_docker
	enable_lightdm
	change_ownership
}

case "${1}" in
	'minimal' )
		install_minimal ;;
	'desktop' )
		install_minimal && install_desktop ;;
	* )
		echo "usage: '# sh install.sh [minimal, desktop]'" ;;
esac
