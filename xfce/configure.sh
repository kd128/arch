#!/bin/sh

configure_appearance() {
	xfconf-query --channel xsettings --property "/Gtk/ButtonImages" --create --type bool --set false
	xfconf-query --channel xsettings --property "/Gtk/DecorationLayout" --create --type string --set ":minimize,maximize,close"
	xfconf-query --channel xsettings --property "/Net/IconThemeName" --create --type string --set "Papirus-Dark"
	xfconf-query --channel xsettings --property "/Net/ThemeName" --create --type string --set "Adwaita-dark"
	xfconf-query --channel xsettings --property "/Xfce/SyncThemes" --create --type bool --set true
	xfconf-query --channel xsettings --property "/Xft/RGBA" --create --type string --set "rgb"
}

configure_desktop() {
	xfconf-query --channel xfce4-desktop --property "/desktop-icons/show-thumbnails" --create --type bool --set false
	xfconf-query --channel xfce4-desktop --property "/desktop-icons/show-tooltips" --create --type bool --set false
	xfconf-query --channel xfce4-desktop --property "/desktop-icons/style" --create --type int --set 0
	xfconf-query --channel xfce4-desktop --property "/desktop-menu/show" --create --type bool --set false
	xfconf-query --channel xfce4-desktop --property "/desktop-menu/show-delete" --create --type bool --set false
	xfconf-query --channel xfce4-desktop --property "/windowlist-menu/show" --create --type bool --set false
}

configure_thunar() {
	xfconf-query --channel thunar --property "/default-view" --create --type string --set "ThunarDetailsView"
	xfconf-query --channel thunar --property "/last-side-pane" --create --type string --set "ThunarTreePane"
	xfconf-query --channel thunar --property "/misc-middle-click-in-tab" --create --type bool --set true
	xfconf-query --channel thunar --property "/misc-single-click" --create --type bool --set false
	xfconf-query --channel thunar --property "/misc-thumbnail-draw-frames" --create --type bool --set true
	xfconf-query --channel thunar --property "/misc-volume-management" --create --type bool --set false
}

configure_notification() {
	xfconf-query --channel xfce4-notifyd --property "/mute-sounds" --create --type bool --set true
	xfconf-query --channel xfce4-notifyd --property "/do-fadeout" --create --type bool --set false
}

configure_window_manager() {
	xfconf-query --channel xfwm4 --property "/general/button_layout" --create --type string --set "T|HMC"
	xfconf-query --channel xfwm4 --property "/general/easy_click" --create --type string --set "None"
	xfconf-query --channel xfwm4 --property "/general/scroll_workspaces" --create --type bool --set false
	xfconf-query --channel xfwm4 --property "/general/wrap_windows" --create --type bool --set false
}

configure_keyboard() {
	xfconf-query --channel keyboards --property "/Default/KeyRepeat/Delay" --create --type int --set 200
	xfconf-query --channel keyboards --property "/Default/KeyRepeat/Rate" --create --type int --set 30
	xfconf-query --channel keyboards --property "/Default/RestoreNumlock" --create --type bool --set true
}

configure_terminal() {
	xfconf-query --channel xfce4-terminal --property "/font-use-system" --create --type bool --set true
	xfconf-query --channel xfce4-terminal --property "/misc-show-unsafe-paste-dialog" --create --type bool --set false
	xfconf-query --channel xfce4-terminal --property "/shortcuts-no-menukey" --create --type bool --set true
	xfconf-query --channel xfce4-terminal --property "/shortcuts-no-mnemonics" --create --type bool --set true
	xfconf-query --channel xfce4-terminal --property "/title-mode" --create --type string --set "TERMINAL_TITLE_HIDE"
}

configure_shortcuts() {
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/commands/custom/<Alt><Super>s" --reset
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/commands/custom/<Alt>F1" --reset
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/commands/custom/<Alt>F2" --reset
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/commands/custom/<Alt>F3" --reset
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/commands/custom/<Alt>Print" --reset
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/commands/custom/<Alt>p" --create --type string --set "/usr/bin/xfce4-appfinder"
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/commands/custom/<Primary><Alt>Delete" --reset
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/commands/custom/<Primary><Alt>Escape" --reset
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/commands/custom/<Primary><Alt>f" --reset
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/commands/custom/<Primary><Alt>l" --reset
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/commands/custom/<Primary><Alt>t" --reset
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/commands/custom/<Primary><Shift>Escape" --reset
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/commands/custom/<Primary>Escape" --reset
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/commands/custom/<Primary>Print" --create --type string --set "sh -c 'scrot --select --file ~/pictures/screenshots/%y%m%d_%H%M%S.png'"
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/commands/custom/<Shift><Alt>Return" --create --type string --set "/usr/bin/xfce4-terminal"
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/commands/custom/<Shift><Super>at" --create --type string --set "/usr/bin/firefox --private-window"
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/commands/custom/<Shift>Print" --reset
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/commands/custom/<Super>1" --create --type string --set "/usr/bin/thunar"
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/commands/custom/<Super>2" --create --type string --set "/usr/bin/firefox"
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/commands/custom/<Super>e" --reset
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/commands/custom/<Super>l" --create --type string --set "/usr/bin/xflock4"
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/commands/custom/<Super>p" --reset
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/commands/custom/<Super>r" --reset
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/commands/custom/HomePage" --reset
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/commands/custom/Print" --create --type string --set "sh -c 'scrot --file ~/pictures/screenshots/%y%m%d_%H%M%S.png'"
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/commands/custom/XF86Display" --reset
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/commands/custom/XF86Mail" --reset
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/commands/custom/XF86WWW" --reset
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/xfwm4/custom/<Alt><Shift>Tab" --reset
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/xfwm4/custom/<Alt>Delete" --reset
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/xfwm4/custom/<Alt>F10" --reset
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/xfwm4/custom/<Alt>F11" --reset
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/xfwm4/custom/<Alt>F12" --reset
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/xfwm4/custom/<Alt>F6" --reset
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/xfwm4/custom/<Alt>F7" --reset
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/xfwm4/custom/<Alt>F8" --reset
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/xfwm4/custom/<Alt>F9" --reset
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/xfwm4/custom/<Alt>Insert" --reset
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/xfwm4/custom/<Alt>space" --reset
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/xfwm4/custom/<Primary><Alt>Down" --reset
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/xfwm4/custom/<Primary><Alt>End" --reset
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/xfwm4/custom/<Primary><Alt>Home" --reset
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/xfwm4/custom/<Primary><Alt>KP_1" --reset
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/xfwm4/custom/<Primary><Alt>KP_2" --reset
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/xfwm4/custom/<Primary><Alt>KP_3" --reset
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/xfwm4/custom/<Primary><Alt>KP_4" --reset
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/xfwm4/custom/<Primary><Alt>KP_5" --reset
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/xfwm4/custom/<Primary><Alt>KP_6" --reset
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/xfwm4/custom/<Primary><Alt>KP_7" --reset
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/xfwm4/custom/<Primary><Alt>KP_8" --reset
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/xfwm4/custom/<Primary><Alt>KP_9" --reset
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/xfwm4/custom/<Primary><Alt>Left" --reset
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/xfwm4/custom/<Primary><Alt>Right" --reset
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/xfwm4/custom/<Primary><Alt>Up" --reset
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/xfwm4/custom/<Primary><Alt>d" --reset
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/xfwm4/custom/<Primary><Super>Left" --create --type string --set "prev_workspace_key"
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/xfwm4/custom/<Primary><Super>Right" --create --type string --set "next_workspace_key"
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/xfwm4/custom/<Primary>F1" --reset
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/xfwm4/custom/<Primary>F10" --reset
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/xfwm4/custom/<Primary>F11" --reset
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/xfwm4/custom/<Primary>F12" --reset
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/xfwm4/custom/<Primary>F2" --reset
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/xfwm4/custom/<Primary>F3" --reset
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/xfwm4/custom/<Primary>F4" --reset
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/xfwm4/custom/<Primary>F5" --reset
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/xfwm4/custom/<Primary>F6" --reset
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/xfwm4/custom/<Primary>F7" --reset
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/xfwm4/custom/<Primary>F8" --reset
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/xfwm4/custom/<Primary>F9" --reset
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/xfwm4/custom/<Shift><Alt>Page_Down" --reset
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/xfwm4/custom/<Shift><Alt>Page_Up" --reset
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/xfwm4/custom/<Shift><Super>Left" --create --type string --set "move_window_prev_workspace_key"
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/xfwm4/custom/<Shift><Super>Right" --create --type string --set "move_window_next_workspace_key"
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/xfwm4/custom/<Super>KP_Down" --reset
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/xfwm4/custom/<Super>KP_End" --reset
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/xfwm4/custom/<Super>KP_Home" --reset
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/xfwm4/custom/<Super>KP_Left" --reset
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/xfwm4/custom/<Super>KP_Next" --reset
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/xfwm4/custom/<Super>KP_Page_Up" --reset
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/xfwm4/custom/<Super>KP_Right" --reset
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/xfwm4/custom/<Super>KP_Up" --reset
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/xfwm4/custom/<Super>Left" --create --type string --set "tile_left_key"
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/xfwm4/custom/<Super>Right" --create --type string --set "tile_right_key"
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/xfwm4/custom/<Super>Up" --create --type string --set "maximize_window_key"
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/xfwm4/custom/<Super>d" --create --type string --set "show_desktop_key"
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/xfwm4/custom/Down" --reset
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/xfwm4/custom/Left" --reset
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/xfwm4/custom/Right" --reset
	xfconf-query --channel xfce4-keyboard-shortcuts --property "/xfwm4/custom/Up" --reset
}

configure_appearance
configure_desktop
configure_thunar
configure_notification
configure_window_manager
configure_keyboard
configure_terminal
configure_shortcuts
