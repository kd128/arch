local g = vim.g
g.loaded_node_provider = 0
g.loaded_perl_provider = 0
g.loaded_python3_provider = 0
g.loaded_ruby_provider = 0

local o = vim.o
o.hlsearch = false
o.ignorecase = true
o.number = true
o.showmode = false
o.statusline = " %{mode()} %f %m %= %l %c "
o.wrap = false
