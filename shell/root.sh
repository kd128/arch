#!/bin/sh

export PS1="\[\e[101m\] \[\e[30m\]\W\[\e[39m\] \[\e[0m\] "
export HISTCONTROL="ignoreboth"
export LESSHISTFILE="/dev/null"

alias ls="ls --color --group-directories-first"
alias ll="ls --color --group-directories-first --all --human-readable -l"
alias ip="ip --color"
alias grep="grep --color --ignore-case"
alias rm="rm --interactive"
alias cp="cp --interactive"
alias mv="mv --interactive"
alias mkdir="mkdir --parents"
