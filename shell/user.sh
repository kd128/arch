#!/bin/sh

[ "${-}" = "*i*" ] && return

export PS1="\[\e[106m\] \[\e[30m\]\W\[\e[39m\] \[\e[0m\] "
export HISTCONTROL="ignoreboth"
export LESSHISTFILE="/dev/null"
export EDITOR="nvim"
export GOPATH="${HOME}/documents/go"

alias ls="ls --color --group-directories-first"
alias ll="ls --color --group-directories-first --all --human-readable -l"
alias ip="ip --color"
alias grep="grep --color --ignore-case"
alias rm="rm --interactive"
alias cp="cp --interactive"
alias mv="mv --interactive"
alias mkdir="mkdir --parents"
alias tree="tree -C -a --dirsfirst --gitignore"
alias qmv="qmv --format='destination-only'"
